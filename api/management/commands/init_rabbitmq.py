import logging.config

import pika
from django.conf import settings
from django.core.management import BaseCommand

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


class Command(BaseCommand):
    def handle(self, *args, **options):
        log.info("Initializing RabbitMQ")
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=settings.RABBITMQ_HOST,
            port=settings.RABBITMQ_PORT,
            credentials=pika.PlainCredentials(settings.RABBITMQ_USER,
                                              settings.RABBITMQ_PASSWORD)
        ))
        channel = connection.channel()
        channel.queue_declare(queue='catalyst')
        channel.exchange_declare(exchange='catalyst.sla.notifications',
                                 exchange_type='direct')
        connection.close()
        log.info("Initialized RabbitMQ")