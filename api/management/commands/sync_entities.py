import logging.config
import os

from django.conf import settings
from django.core.management import BaseCommand

from api.clients.vcg.client import VCG

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


class Command(BaseCommand):
    def handle(self, *args, **options):
        log.info("Synching all vc_tags from {}".format(os.getenv('VCG_API_SERVICE')))
        vcg = VCG(url=os.getenv('VCG_API_SERVICE'))
        vc_tags = vcg.get_vc_tags()
        for vc_tag in vc_tags:
            vcg.sync_events_for_vc(vc_tag=vc_tag)
        log.info("Done.")
