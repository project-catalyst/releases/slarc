import logging.config

from django.conf import settings
from django.core import management
from django.core.management import BaseCommand, call_command

from api.models import Defaults

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


class Command(BaseCommand):
    def handle(self, *args, **options):
        log.info("Checking whether the database should be initialized..")
        defaults = Defaults.objects.all()
        if defaults.count() == 0:
            log.info("Loading initial data..")
            call_command('loaddata', 'fixtures/init.json')
            log.info("Done")
        else:
            log.info("No need to initialize the database.")
        management.call_command('create_superuser', verbosity=0)
        return
