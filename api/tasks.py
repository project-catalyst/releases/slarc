import logging.config
import time

from celery.contrib.abortable import AbortableTask, AbortableAsyncResult
from django.conf import settings
from django.core.cache import cache
from django.utils import timezone
from rest_framework.renderers import JSONRenderer

from api.clients.notify.client import Notifier
from api.models import SLO, Task, Event, ClientToNotify, SLA
from api.serializers import SLASerializer
from api.utils import reset_slo_value
from slarc import celery_app as app

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


@app.task(bind=True, base=AbortableTask)
def event_timer(self, event, sleep=1):
    """

    Args:
        self:
        event: The event to base on
        sleep: The sleep time between calculations

    Returns:

    """
    slo = SLO.objects.get(id=event["slo"])
    if slo.status == 'completed':
        log.debug(f"Ignoring event {event['id']} since {slo} is marked as completed..")
        return

    event_obj = Event.objects.get(id=event["id"])
    initial_event_value = (timezone.now().replace(tzinfo=timezone.utc) - event_obj.start_time.replace(tzinfo=timezone.utc)).total_seconds()
    log.debug("initial_event_value is {} seconds".format(initial_event_value))
    event_obj.add_value(initial_event_value)
    task = Task.objects.get(task_id=self.request.id)
    while not self.is_aborted():
        try:
            if not cache.get("lock-{}".format(event_obj.id)):
                event_obj.add_value(sleep)
            else:
                log.debug("Skipping event update calculation (event id: {}) due to locking.".format(event_obj.id))
            time.sleep(sleep)
            if not slo.update_levels():
                log.warning("Could not update levels of SLO {}. Revoking..".format(slo))
                task.status = 'aborted'
                task.save()
                AbortableAsyncResult(task.task_id).abort()
                return
            log.debug("Still calculating.")
        except Exception as e:
            log.warning("An error occurred while processing event {}. Revoking..".format(event["id"]))
            log.error(e)
            task.status = 'aborted'
            task.save()
            AbortableAsyncResult(task.task_id).abort()
            return

    event = task.event
    event.value = 0
    event.save()
    event.add_value((event.end_time.replace(tzinfo=timezone.utc) - event.start_time.replace(tzinfo=timezone.utc)).total_seconds())
    event.status = 'completed'
    event.save()
    log.debug("Added completed event {}, value is {}".format(event.id, event.value))

    task.status = 'completed'
    task.aborted = timezone.now()
    task.save()
    # Uncomment these values if you have another mechanism for understanding when to end the event
    # task.event.end_time = timezone.now()
    # task.event.status = 'completed'
    # task.event.save()
    log.debug("Gracefully aborted!")
    return


@app.task
def check_sla_end(**kwargs):
    log.debug("Checking for stale SLAs..")
    for sla in SLA.objects.all():
        if sla.status == 'active' and sla.valid_until <= timezone.now():
            sla.status = 'completed'
            sla.save()
            log.debug(f"SLA {sla.id} is valid until {sla.valid_until}. Marked as completed, now updating SLOs..")
            for slo in SLO.objects.filter(sla=sla):
                slo.status = 'completed'
                slo.calculate_value()


@app.task
def check_slo_nullification(**kwargs):
    """
    Checks whether there are SLOs whose values should be nullified
    Returns:
        None
    """
    reset_slo_value(kwargs["billing_cyle"])


@app.task
def notify_for_sla_breakage():
    for slo in SLO.objects.all().exclude(status='completed'):
        if slo.target_value == 0:
            notify.apply_async(kwargs={"slo": slo})
        else:
            sla = slo.sla
            list_to_be_notified = ClientToNotify.objects.filter(sla=sla)
            for to_be_notified in list_to_be_notified:
                if to_be_notified.percentage <= 100 * (slo.current_value / slo.target_value):
                    notify.apply_async(kwargs={"sla": str(sla.id)})
    return


@app.task
def notify(**kwargs):
    log.info("Notifying for breakage of SLA {}".format(kwargs["sla"]))
    sla = SLA.objects.get(id=kwargs['sla'])
    for ctn in ClientToNotify.objects.filter(sla=sla):
        log.debug("Notifying {}.".format(ctn.responsible))
        notifier = Notifier(ctn.protocol, ctn.server, ctn.port, ctn.service_url)
        notifier.notify(JSONRenderer().render(SLASerializer(sla).data))
        log.debug("Done")
