import logging.config
import os

from django.conf import settings
from rest_framework import generics
from rest_framework.response import Response

from api.clients.vcg.client import VCG
from api.clients.vcg.exceptions import VCGDataException
from api.models import Defaults
from api.serializers import *

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


class SLAList(generics.ListCreateAPIView):
    queryset = SLA.objects.all()
    serializer_class = SLASerializer


class SLADetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = SLA.objects.all()
    serializer_class = SLASerializer


class SLOList(generics.ListCreateAPIView):
    queryset = SLO.objects.all()
    serializer_class = SLOSerializer


class SLODetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = SLO.objects.all()
    serializer_class = SLOSerializer


class LevelList(generics.ListCreateAPIView):
    queryset = Level.objects.all()
    serializer_class = LevelSerializer


class LevelDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Level.objects.all()
    serializer_class = LevelSerializer


class EventList(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class EventDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class SLAByEntityID(generics.ListAPIView):
    """
    Get the SLA of a VCTag.
    """
    queryset = SLA.objects.all()
    serializer_class = SLASerializer

    def list(self, request, *args, **kwargs):
        entity_type = kwargs['entity_type']
        entity_id = kwargs['entity_id']
        if kwargs['entity_type'] == 'uuid':
            entity_type = 'vc_tag'
            try:
                vc_tags = VCG(url=os.getenv('VCG_API_SERVICE')).get_vc_tags(entity_id)
                entity_id = vc_tags[0]
            except Exception as e:
                log.exception(e)
        try:
            q = self.get_queryset().filter(entity_type=entity_type, entity_id=entity_id)
            ret = self.get_serializer(q, many=True)
            if q.count() == 0:
                return Response(data={"error": "No SLAs found matching the given criteria"}, status=404)
            return Response(data=ret.data, status=200)
        except Exception as e:
            log.exception(e)
            return Response(data={"error": str(e)}, status=500)


class ExternalComponents(generics.CreateAPIView):
    """
    Consume events from external components
    """
    serializer_class = GeneralSerializer

    def create(self, request, *args, **kwargs):
        component = kwargs['component']
        if "vcg" == component:
            client = VCG(url=os.getenv('VCG_API_SERVICE'))
            try:
                client.handle_event(request.data)
                return Response(status=201)
            except VCGDataException as e:
                log.exception(e)
                return Response(data={"error": (e)}, status=400)
            except Exception as e:
                log.exception(e)
                return Response(data={"error": str(e)}, status=500)
        else:
            return Response(data={"error": "Service {} not found".format(component)}, status=404)


class DefaultsView(generics.ListCreateAPIView):
    """
    Show/Create the server defaults
    """
    queryset = Defaults.objects.all()
    serializer_class = DefaultsSerializer

    def list(self, request, *args, **kwargs):
        q = self.get_queryset().last()
        ret = self.get_serializer(q, many=False)
        return Response(data=ret.data, status=200)


class ClientsView(generics.ListCreateAPIView):
    queryset = ClientToNotify.objects.all()
    serializer_class = ClientToNotifySerializer


class ClientsDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ClientToNotify.objects.all()
    serializer_class = ClientToNotifySerializer