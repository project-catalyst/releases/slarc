from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from api.views import *

schema_view = get_schema_view(
    openapi.Info(
        title="CATALYST SLA Controller API",
        default_version='v0.1',
        description="A reference implementation of the H2020 CATALYST project SLA renegotiation controller component. Note that no re-negotiation is taking place currently.",
        contact=openapi.Contact(email="artemis@power-ops.com"),
        license=openapi.License(name="LGPL License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('slas/', SLAList.as_view(), name="List SLAs"),
    path('slas/<str:pk>', SLADetails.as_view(), name="SLA details"),
    path('slas/by-type/<str:entity_type>/<str:entity_id>', SLAByEntityID.as_view(), name="SLAs for entity"),

    path('slos/', SLOList.as_view(), name="List SLOs"),
    path('slos/<str:pk>', SLODetails.as_view(), name="SLO details"),

    path('levels/', LevelList.as_view(), name="List levels"),
    path('levels/<str:pk>', LevelDetails.as_view(), name="Level details"),

    path('events/', EventList.as_view(), name="List Events"),
    path('events/external/<str:component>', ExternalComponents.as_view(),
         name="Create events from external components"),
    path('events/<str:pk>', EventDetails.as_view(), name="Event details"),

    path('defaults/', DefaultsView.as_view(), name="List defaults"),

    path('notify/clients/', ClientsView.as_view(), name="List/Define notification clients"),
    path('notify/clients/<str:pk>', ClientsView.as_view(), name="Retrieve/Update/Destroy notification clients"),

    path('docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
