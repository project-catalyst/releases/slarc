import logging
import uuid

from celery.contrib.abortable import AbortableAsyncResult
from django.db import models
from django.db.models import Q, Sum
from django.utils import timezone

log = logging.getLogger("api")


class SLA(models.Model):
    SLA_STATUS = [
        ('active', 'Active'),
        ('completed', 'Completed')
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="The id of the SLA")
    valid_from = models.DateTimeField(help_text="The activation time of the SLA")
    valid_until = models.DateTimeField(help_text="The deactivation time of the SLA")
    entity_type = models.CharField(max_length=100, null=False, blank=False, help_text="The entity type to watch")
    entity_id = models.CharField(max_length=100, null=False, blank=False, help_text="The entity type to watch")
    status = models.CharField(max_length=9, choices=SLA_STATUS, default='active', null=False, blank=False,
                              help_text="The SLA status")

    def __str__(self):
        return "SLA ({}) for entity {} {}".format(self.id, self.entity_type, self.entity_id)


class SLO(models.Model):
    OBJECTIVE_CHOICES = [
        ('downtime', 'Downtime'),
        ('brown_hosting', 'Hosting in non-green-powered (brown) servers')
    ]
    BILLING_CYCLE = [
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('monthly', 'Monthly'),
        ('yearly', 'Yearly')
    ]
    UNITS = [
        ('seconds', 'Seconds'),
        ('minutes', 'Minutes'),
        ('hours', 'Hours')
    ]
    SLO_STATUS = [
        ('ok', 'OK'),
        ('broken', 'Broken'),
        ('completed', 'Completed')
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="The id of the SLO")
    objective = models.CharField(max_length=13, choices=OBJECTIVE_CHOICES, default='dt', null=False, blank=False,
                                 help_text="The SLO objective")
    target_value = models.FloatField(default=0, null=False, blank=False,
                                     help_text="The value that when reached, the SLO is broken")
    current_value = models.FloatField(default=0, null=False, blank=False, help_text="The current SLO value")
    unit = models.CharField(max_length=7, choices=UNITS, default='m', null=False, blank=False,
                            help_text="The units of measurement for the value")
    sla = models.ForeignKey(SLA, on_delete=models.CASCADE, related_name="slos",
                            help_text="The SLA to which this SLO adheres")
    billing_cycle = models.CharField(max_length=7, choices=BILLING_CYCLE, null=False, blank=False, default='m',
                                     help_text="Period for resetting the SLO status")
    last_billed = models.DateTimeField(default=timezone.now, help_text="The last time the billing was calculated")
    next_bill = models.DateTimeField(default=timezone.now, help_text="The next time the billing will be calculated")
    status = models.CharField(max_length=9, choices=SLO_STATUS, default='ok', null=False, blank=False,
                              help_text="The SLO status")

    def __str__(self):
        return f"SLO ({self.id}) (cv: {self.current_value}/{self.target_value} {self.unit})"

    def calculate_value(self, reference_end=None, freeze_ongoing_events=False):
        """
        Calculates the value of the SLO
        Args:
            reference_end: If we need a reference end time (used for billing)
            freeze_ongoing_events: If we need to kill ongoing events (used for billing)

        Returns: (Bool, Value) for (Successful/Unsuccessful operation, current SLO value)

        """
        events = self.get_billing_events(reference_end, freeze_ongoing_events)
        cv = events.aggregate(Sum('value')).get('value__sum', 0) # if events.count() > 0 else 0
        self.current_value = cv
        self.save()
        self.update_levels(recalculate=False)
        log.debug("Updated SLO. CV {}".format(SLO.objects.get(id=self.id).current_value))
        return True, self.current_value

    def update_levels(self, recalculate=True):
        """
        Re-calculates the SLO levels
        Args:
            recalculate: If re-calculation should be enforced

        Returns:

        """
        success = True
        value = self.current_value
        if recalculate:
            success, value = self.calculate_value()
            return success
        if not success:
            log.error("Could not update the status of the SLO.")
            return False
        levels = Level.objects.filter(slo=self).order_by('min_value')
        for level in levels:
            level.status = 'inactive'
            level.save()
        for level in levels:
            if level.max_value < level.min_value <= value:
                # Final level
                level.status = 'active'
                self.status = 'broken'
                level.save()
                self.save()
                break
            elif level.min_value <= value < level.max_value:
                level.status = 'active'
                self.status = 'active'
                level.save()
                self.save()
                break
            else:
                level.status = 'inactive'
                self.status = 'active'
                level.save()
                self.save()
        return True

    def get_billing_events(self, reference_end=None, freeze_ongoing_events=False):
        # Handling of the ongoing events is as follows:
        #   1. Get the ongoing tasks
        #   2. Kill them
        #   3. Start new events for continuing the killed ones
        #
        if freeze_ongoing_events:
            log.info("WTF")
            # Get the ongoing events
            ongoing_events = Event.objects.filter(slo=self, start_time__gt=self.last_billed, end_time__isnull=True)
            for ongoing_event in ongoing_events:
                # Kill ongoing tasks
                # This one should also update and save the event upon completion
                AbortableAsyncResult(Task.objects.get(event=ongoing_event).task_id).abort()
                # Creat new events - This will create a new task for calculation
                Event.objects.create(
                    slo=ongoing_event.slo,
                    start_time=timezone.now(),
                    end_time=None,
                    status='active',
                    type=ongoing_event.type,
                    value=0
                ).save()

        # Now the events
        if reference_end:
            events = Event.objects.filter(slo=self, start_time__gte=self.last_billed).filter(
                Q(end_time__lte=reference_end) | Q(end_time__isnull=True))
        else:
            events = Event.objects.filter(slo=self, start_time__gte=self.last_billed)
        return events


class Level(models.Model):
    STATUS_CHOICES = [
        ('active', 'Active'),
        ('inactive', 'Inactive')
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="The id of the SLO Level")
    min_value = models.FloatField(default=0, null=False, blank=False,
                                  help_text="The minimum value that when reached, the level is activated")
    max_value = models.FloatField(default=0, null=False, blank=False,
                                  help_text="The maximum value that when reached, the level is deactivated")
    price_drop_percentage = models.FloatField(default=0, null=False, blank=False, help_text="The price drop percentage")
    slo = models.ForeignKey(SLO, on_delete=models.CASCADE, related_name="levels",
                            help_text="The SLO that the level refers to")
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, default="inactive", null=False, blank=False,
                              help_text="The status of the level")

    def __str__(self):
        return f"{self.status} level for {self.slo} ({self.min_value}, {self.max_value})"


class Event(models.Model):
    STATUS_CHOICES = [
        ('active', 'Active'),
        ('completed', 'Completed'),
        ('inactive', 'Inactive')
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, help_text="The id of the event")
    slo = models.ForeignKey(SLO, on_delete=models.CASCADE, related_name="events",
                            help_text="The SLO that the event refers to")
    type = models.CharField(max_length=13, choices=SLO.OBJECTIVE_CHOICES, default="dt", null=False, blank=False,
                            help_text="The type of the event")
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, default="active", null=False, blank=False,
                              help_text="The status of the event")
    start_time = models.DateTimeField(help_text="The start time of the event")
    end_time = models.DateTimeField(null=True, blank=True, help_text="The end time of the event")
    value = models.FloatField(default=0, help_text="The current value of the event as to the event type")

    def __str__(self):
        return f"Event ({self.id}) for {self.slo}. Value is {self.value}"

    class Meta:
        ordering = ["start_time", "end_time"]

    def add_value(self, value):
        """
        Updates the value of the event
        Args:
            value: The value to add, in seconds

        Returns: None

        """
        multiplier = float(1)
        if self.slo.unit == "minutes":
            multiplier = float(1.0 / 60.0)
        elif self.slo.unit == "hours":
            multiplier = float(1.0 / 3600.0)
        self.value = self.value + (value * multiplier)
        self.save()
        self.slo.calculate_value()
        log.debug("Added {} {} to SLO {} value. CV is {}. Event value is {}.".format(value * multiplier, self.slo.unit, self.slo.id, self.slo.current_value, self.value))


class Task(models.Model):
    STATUS_CHOICES = [
        ('active', 'active'),
        ('completed', 'completed'),
        ('aborted', 'aborted')
    ]
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False,
                          help_text="The (internal) id of the task")
    task_id = models.CharField(max_length=36, null=False, blank=False, help_text="The Task id")
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True, blank=True, help_text="Related event")
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, default="active", null=False, blank=False,
                              help_text="The status of the task")
    created = models.DateTimeField(default=timezone.now, help_text="When was the task created")
    aborted = models.DateTimeField(null=True, blank=True, help_text="When was the task aborted")


class SLOBillingHistory(models.Model):
    SLO_STATUS = [
        ('ok', 'OK'),
        ('broken', 'Broken'),
        ('completed', 'Completed')
    ]
    slo = models.ForeignKey(SLO, on_delete=models.CASCADE, related_name="history",
                            help_text="The SLO the the history refers to")
    billing_time = models.DateTimeField(default=timezone.now, help_text="The time the billing was calculated")
    start = models.DateTimeField(help_text="The start time of the billing cycle")
    end = models.DateTimeField(help_text="The end time of the billing cycle")
    value = models.FloatField(default=0, null=False, blank=False, help_text="The SLO value during this cycle")
    status = models.CharField(max_length=6, choices=SLO_STATUS, default='ok', null=False, blank=False,
                              help_text="The SLO status during this period")


class ClientToNotify(models.Model):
    CLIENT_TYPES = [
        ('amqp', 'RabbitMQ'),
        ('http', 'HTTP'),
        ('https', 'HTTPS'),
        ('kafka', 'kafka')
    ]
    responsible = models.CharField(max_length=100, help_text="Who is the one being notified")
    protocol = models.CharField(max_length=5, choices=CLIENT_TYPES, default='http', null=False, blank=False,
                                help_text="The protocol of the notification")
    server = models.CharField(max_length=100, blank=False, null=False,
                              help_text="The server to send the information to")
    port = models.IntegerField(default=443, help_text="The port to connect to")
    service_url = models.CharField(max_length=512, blank=False, null=False,
                                   help_text="The rest of the URL to send the information to")
    sla = models.ForeignKey(SLA, on_delete=models.CASCADE, null=True, blank=True,
                            help_text="The SLA to send notification for")
    percentage = models.FloatField(default=80.0, null=False, blank=False,
                                   help_text="The percentage of the SLA's SLOs that when reached the notification will be sent")
    last_notified = models.DateTimeField(help_text="When was the last notification sent.")


class Defaults(models.Model):
    SLA_DURATION = [
        ('year', 'One Year'),
        ('month', 'One Month'),
        ('week', 'One Week')
    ]
    slo_billing_cycle = models.CharField(max_length=7, choices=SLO.BILLING_CYCLE, default='monthly', blank=False,
                                         null=False, help_text="Default SLO billing cycle")
    slo_objective = models.CharField(max_length=13, choices=SLO.OBJECTIVE_CHOICES, default='dt', null=False,
                                     blank=False, help_text="The default SLO objective")
    slo_target_value = models.FloatField(default=0, null=False, blank=False,
                                         help_text="The default value that when reached, the SLO is broken")
    slo_unit = models.CharField(max_length=7, choices=SLO.UNITS, default='m', null=False, blank=False,
                                help_text="The default units of measurement for the value")
    ctn_protocol = models.CharField(max_length=5, choices=ClientToNotify.CLIENT_TYPES, default='http', null=False,
                                    blank=False, help_text="The default protocol of the notification")
    ctn_server = models.CharField(max_length=100, blank=False, null=False,
                                  help_text="The default server to send the information to")
    ctn_port = models.IntegerField(default=443, help_text="The default port to connect to")
    ctn_service_url = models.CharField(max_length=512, blank=False, null=False,
                                       help_text="The default rest of the URL to send the information to")
    ctn_percentage = models.FloatField(default=80.0, null=False, blank=False,
                                       help_text="The default percentage of the SLA's SLOs that when reached the notification will be sent")
    sla_duration = models.CharField(max_length=5, choices=SLA_DURATION, default='year', null=False, blank=False,
                                    help_text="The default SLA duration")
