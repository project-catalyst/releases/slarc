import logging.config

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from api.models import Event, Task
from api.serializers import EventSerializer
from api.tasks import event_timer

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


@receiver(post_save, sender=Event)
def handle_event_create(sender, **kwargs):
    event = kwargs["instance"]
    if not kwargs["created"]:
        return

    # if event.end_time and event.start_time:
    #     log.info(event)
    #     Event.objects.filter(id=event.id).update(value=0)
    #     event.add_value((event.end_time.replace(tzinfo=timezone.utc) - event.start_time.replace(tzinfo=timezone.utc)).total_seconds())
    #     log.debug("Added completed event {}, value is {}".format(event.id, Event.objects.get(id=event.id).value))
    #     Event.objects.filter(id=event.id).update(status='completed')
    # else:
    log.info("Handling event")
    try:
        task = Task.objects.get(event__in=Event.objects.filter(slo=event.slo), status='active')
        if task:
            log.debug("There is already a task monitoring this event..")
            return
        else:
            log.warning("No task")
    except Exception as e:
        log.error(e)

    log.debug("Spawning new task to monitor event {}".format(event.id))
    timer = event_timer.apply_async(kwargs={"event": EventSerializer(event).data, "sleep": 1})
    log.debug("Done. Creating new task object..")
    task = Task.objects.create(
        task_id=str(timer),
        event=event
    )
    task.save()
    log.debug("Done")

# @receiver(post_save, sender=Task)
# def handle_task_save(sender, **kwargs):
#     task = kwargs['instance']
#     if not kwargs['created']:
#         return
#
#     log.info("Handling task end")
#     event = task.event
#     # Event.objects.filter(id=event.id).update(value=0)
#     event.value = (event.end_time.replace(tzinfo=timezone.utc) - event.start_time.replace(tzinfo=timezone.utc)).total_seconds()
#     event.status = 'completed'
#     event.save()
#     # event.add_value(
#     #     (event.end_time.replace(tzinfo=timezone.utc) - event.start_time.replace(tzinfo=timezone.utc)).total_seconds())
#     log.debug("Added completed event {}, value is {}".format(event.id, event.value))
#     # Event.objects.filter(id=event.id).update(status='completed')