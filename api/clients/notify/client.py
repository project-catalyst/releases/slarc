from api.clients.notify.handlers import http, rabbitmq, kafka


class Notifier(object):
    def __init__(self, protocol, server, port, relative_url, auth=None):
        self.protocol = protocol
        self.server = server
        self.port = port
        self.relative_url = relative_url
        self.auth = auth
        self.handler = None

        if self.protocol == "http":
            self._set_handler(http)
        elif self.protocol == "amqp":
            self._set_handler(rabbitmq)
        elif self.protocol == "kafka":
            self._set_handler(kafka)

    def _set_handler(self, handler):
        """
        Sets the default handler
        Args:
            handler: The handler to use for notifying

        Returns:

        """
        self.handler = handler.Handler()

    def notify(self, data):
        self.handler.handle(self.protocol, self.server, self.port, self.relative_url, self.auth, data)
