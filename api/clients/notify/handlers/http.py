import urllib.parse

import requests

from api.clients.notify.handlers.abstract_handler import AbstractHandler, log


class Handler(AbstractHandler):

    def handle(self, protocol, server, port, service, auth, data):
        """
        Notifies a user
        Args:
            protocol: The protocol to use
            server: The server to use
            port: The port to use
            service: The relative url
            auth: The authentication module (This is still ongoing)
            data: The data set to send

        Returns:
            True if everything went well, False otherwise
        """
        try:
            url = urllib.parse.urljoin("{}://{}:{}".format(protocol, server, port), service)
            res = requests.post(url=url,
                                headers={"Content-Type": "application/json"},
                                data=data)
            res.raise_for_status()
            if res.status_code in [200, 201, 204]:
                return True
            else:
                return False
        except Exception as e:
            log.error(e)
            return False
