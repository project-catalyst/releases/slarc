import abc
import logging.config

from django.conf import settings

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


class AbstractHandler(abc.ABC):

    @abc.abstractmethod
    def handle(self, protocol, server, port, service, auth, data):
        pass
