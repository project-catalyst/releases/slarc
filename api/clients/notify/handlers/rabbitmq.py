import pika
from django.conf import settings

from api.clients.notify.handlers.abstract_handler import AbstractHandler, log


class Handler(AbstractHandler):

    def handle(self, protocol, server, port, service, auth, data):
        """
        Notifies a user via rabbitmqt
        Args:
            protocol: The protocol to use (amqp)
            server: The server to use
            port: The port to use
            service: The queue to post
            auth: The authentication module (This is still ongoing)
            data: The data set to send

        Returns:
            True if everything went well, False otherwise
        """
        try:
            connection = pika.BlockingConnection(pika.ConnectionParameters(
                host=server,
                port=port,
                credentials=pika.PlainCredentials(settings.RABBITMQ_USER,
                                                  settings.RABBITMQ_PASSWORD)
            ))
            channel = connection.channel()
            channel.queue_declare(queue=service)
            channel.basic_publish(exchange='catalyst.sla.notifications',
                                  routing_key='breakage_notification',
                                  body=data)
            connection.close()
            return True
        except Exception as e:
            log.error(e)
            return False
