from kafka import KafkaProducer
from kafka.errors import KafkaError

from api.clients.notify.handlers.abstract_handler import AbstractHandler, log


class Handler(AbstractHandler):
    def handle(self, protocol, server, port, service, auth, data):
        """
        Notifies through a kafka service
        Args:
            protocol: Should be "kafka" (ignored in all cases)
            server: The KAFKA boostrap server
            port: The KAFKA boostrap server port
            service: The KAFKA topic
            auth: Ignored for now
            data: The data to send

        Returns:
            Nothing
        """
        producer = KafkaProducer(bootstrap_servers="{}:{}".format(server, port),
                                 api_version=(1, 1, 0))
        try:
            res = producer.send(topic=service,
                                value=data)
            try:
                # set timeout in 5 sec
                res.get(timeout=5)
            except KafkaError as ke:
                log.exception(ke)
        except Exception as e:
            log.exception(e)

        try:
            producer.close(timeout=5)
        except Exception as e:
            log.exception(e)
