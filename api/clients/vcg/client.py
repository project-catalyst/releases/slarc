import json
import logging.config
from urllib.parse import urljoin

import requests
from django.conf import settings
from jsonschema import validate, ValidationError

from api.clients.vcg.exceptions import VCGDataException
from api.clients.vcg.handlers import availability_change
from api.clients.vcg.schemas import event_schema
from api.models import SLA, SLO
from api.utils import create_default_chain_for_entity

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger(__name__)


class VCG(object):

    def __init__(self, url=None):
        self.url = url
        self.endpoints = {
            'containers_history': {
                'method': 'GET',
                'rel'   : '/api/container/{vc}/history/'
            },
            'containers_all'    : {
                'method': 'GET',
                'rel'   : '/api/container/all/'
            },
            'container_by_uuid':{
                'method': 'GET',
                'rel': '/api/container/by-id/{uuid}/'
            }
        }

    def get_vc_tags(self, uuid=None):
        if not uuid:
            url = urljoin(self.url, self.endpoints['containers_all']['rel'])
        else:
            url = urljoin(self.url, self.endpoints['container_by_uuid']['rel']).replace("{uuid}", uuid)
        res = requests.get(url=url,
                           headers={"Accept": "application/json"})
        res.raise_for_status()
        return res.json()["vc_tags"]

    def sync_events_for_vc(self, vc_tag, json_object=None):
        """
        Syncs SLARC with VCG
        Args:
            vc_tag: The vc_tag to sync for
            json_object: Obsolete (only for testing)

        Returns:

        """
        if not json_object:
            url = urljoin(self.url, self.endpoints['containers_history']['rel'].replace("{vc}", vc_tag))
            res = requests.get(url=url,
                               headers={"Accept": "application/json"})
            res.raise_for_status()
            history = res.json()
        else:
            history = json_object
        handler = None
        for item in history:
            self.handle_event(item)
        return True

    def handle_event(self, item):
        """
        Handles a VCG event
        Args:
            item: The item to handle

        Returns:

        """
        try:
            validate(instance=item, schema=event_schema)
        except ValidationError as e:
            raise VCGDataException("Could not validate VCG History data,", e)
        item = json.loads(item)
        vc_tag = item['args']['vcTag']
        self._check_for_existing_chain("vc_tag", vc_tag, valid_from=item['args']['timestamp'])
        slos = SLO.objects.filter(sla=SLA.objects.get(entity_type="vc_tag", entity_id=vc_tag))
        if item["name"] == "ContainerRegistered":
            handler = None
            # Nothing else to do, it should already have been registered
        elif item["name"] == "ContainerAvailabilityChanged":
            handler = availability_change.handle
            slo = slos.filter(objective='downtime').last()
        elif item["name"] == "ContainerMigrationPending":
            # TODO
            pass
        elif item["name"] == "ContainerMigrated":
            # TODO
            pass
        else:
            raise VCGDataException(f"Unknown event type {item['name']}", None)
        if handler:
            handler(item, slo)

    def _check_for_existing_chain(self, entity_type, entity_id, valid_from=None):
        try:
            SLA.objects.get(entity_type=entity_type, entity_id=entity_id)
            return True
        except SLA.DoesNotExist:
            log.warning("Tag {} does not have a matching SLA stack. Creating a default one.".format(entity_id))
            create_default_chain_for_entity(entity_type=entity_type, entity_id=entity_id, valid_from=valid_from)
