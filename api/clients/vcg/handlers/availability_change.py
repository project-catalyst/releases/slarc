import logging.config
from datetime import timedelta

from celery.contrib.abortable import AbortableAsyncResult
from django.conf import settings
from django.core.cache import cache
from django.utils import timezone

from api.models import Event, Task
from external.models import VCGEvent
from external.utils import save_vcg_event, vcg_event_to_dict
from slarc import celery_app as app

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger(__name__)


def _handle_multiple_events(events):
    log.warning("INCONSISTENCY: Multiple events found for handing a single object.")
    log.warning("Event IDs are as follows: {}".format([x.id for x in events]))
    log.warning("Attempting to fix inconsistent state.")
    log.warning("Checking for existing tasks..")
    if events.count() < 1:
        log.critical("No event to handle")
        return None

    slo = events[0].slo
    events_to_delete = []
    tasks_to_delete = []
    for event in events:
        for task in Task.objects.filter(event=event):
            log.info("Checking task {}".format(task))
            candidate_celery_task = AbortableAsyncResult(task.task_id)
            if candidate_celery_task.state == "PENDING":
                log.debug("Killing task {}".format(candidate_celery_task))
                candidate_celery_task.abort()
                tasks_to_delete.append(task)
    for task in tasks_to_delete:
        task.delete()
    ret_events = []
    for event in Event.objects.filter(slo=slo):
        if len(Task.objects.filter(event=event)) < 1:
            events_to_delete.append(event)
    for event in events_to_delete:
        log.info("Deleting  stale event {}".format(event.id))
        event.delete()
    all_normal_ongoing_events = Event.objects.filter(slo=slo, end_time__isnull=True)
    if len(all_normal_ongoing_events) != 1:
        log.warning("Found {} relevant events: {}".format(len(ret_events), [x.id for x in ret_events]))
        return None
    return all_normal_ongoing_events[0]


@app.task
def handle(e, slo, recursive=False):
    """
    Checks availability change VCG event against a set of SLOs
    :param e: The VCG event to check
    :param slo: The possibly relevant SLO
    :return:
    """
    if not slo:
        log.error("No valid SLO was provided")
        return None
    if not e:
        log.error("No valid VCG event was provided")
        return None

    vcg_events = VCGEvent.objects.filter(event=None,
                                         block_hash=e['block_hash'],
                                         tx_index=e['tx_index'],
                                         tx_hash=e['tx_hash'],
                                         log_index=e['log_index'],
                                         vc_tag=e['args']['vcTag'],
                                         name=e['name'])

    if not recursive:
        vcg_event = save_vcg_event(e)
    else:
        _candidates = vcg_events.filter(available=e['args']['newStatus'])
        if _candidates.count() == 1:
            vcg_event = _candidates.last()
        elif _candidates.count() == 0:
            log.warning("Could not find a matching VCG Event")
        else:
            log.critical("Houston we have a problem. {}".format(_candidates))

    downtime_detected = not vcg_event.available

    if downtime_detected:
        log.debug("Downtime event detected")
        # Check whether we already know about the event
        log.debug("Checking whether this is stale..")
        if Event.objects.filter(slo=slo, end_time__isnull=True).count() > 0:
            log.debug("There is already another ongoing event, caching it..")
            vcg_event.event = Event.objects.filter(slo=slo, end_time__isnull=True).last()
            vcg_event.save()
            return
        # Check whether we get a
        if Event.objects.filter(slo=slo, start_time=vcg_event.timestamp).count() > 0:
            log.warning("Event already registered, skipping handling")
            log.info("Event acquired: {}".format(e))
            log.info(
                "We found {} relevant events.".format(Event.objects.filter(start_time=vcg_event.timestamp).count()))
            log.info("The events found are: {}".format(Event.objects.filter(slo=slo, start_time=vcg_event.timestamp)))
            vcg_event.event = Event.objects.filter(slo=slo, start_time=vcg_event.timestamp).last()
            vcg_event.save()
            return

        # This is a new event
        log.debug("Creating new event..")
        try:
            new_event = Event.objects.create(
                slo=slo,
                type=slo.objective,
                start_time=vcg_event.timestamp)
            new_event.save()
            vcg_event.event = new_event
            vcg_event.save()

            # Is there a relevant uptime event that came before?
            unhandled_uptime_vcg_events = VCGEvent.objects.filter(event=None,
                                                                  available=True,
                                                                  timestamp__gte=vcg_event.timestamp.replace(
                                                                      tzinfo=timezone.utc) - timedelta(seconds=20))
            if unhandled_uptime_vcg_events.count() > 0:
                log.info("Found {} undhandled VCG events: {}".format(unhandled_uptime_vcg_events.count(),
                                                                     unhandled_uptime_vcg_events))
                for uuve in unhandled_uptime_vcg_events:
                    handle(vcg_event_to_dict(uuve), slo, True)
        except Exception as e:
            log.exception(e)
    else:
        log.debug("Uptime event started")
        # The load revived
        ongoing_events = Event.objects.filter(slo=slo, end_time__isnull=True, status='active')
        # If there is no ongoing event, skip it
        if ongoing_events.count() < 1:
            log.warning("Could not find ongoing started event for {}. Ignoring message.".format(e))
            return
        elif ongoing_events.count() > 1:
            existing_event = _handle_multiple_events(ongoing_events)
        else:
            existing_event = ongoing_events[0]

        if not existing_event:
            log.error("Could not understand which task to handle..")
            return None

        # Find and cancel the running task
        relevant_tasks = Task.objects.filter(event=existing_event)
        if len(relevant_tasks) < 1:
            log.warning("No task was assigned to the previous event, ignoring. Event was {}".format(existing_event))
            handle(e, slo, True)
            return
        cache.set("lock-{}".format(existing_event.id), True)
        for relevant_task in relevant_tasks:
            AbortableAsyncResult(relevant_task.task_id).abort()
            log.debug("Handling task {}".format(relevant_task))
            relevant_task.status = 'completed'
            relevant_task.save()

        existing_event.end_time = vcg_event.timestamp.replace(tzinfo=timezone.utc)
        existing_event.status = 'completed'
        existing_event.value = 0
        existing_event.save()
        existing_event.add_value((existing_event.end_time.replace(
            tzinfo=timezone.utc) - existing_event.start_time.replace(tzinfo=timezone.utc)).total_seconds())
        vcg_event.event = existing_event
        vcg_event.save()
        cache.set("lock-{}".format(existing_event.id), False)
