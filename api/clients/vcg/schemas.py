event_schema = {
    "id"          : "number",
    "name"        : "string",
    "args"        : {
        "type"      : "object",
        "properties": {
            "owner"    : {
                "type": "string"
            },
            "vcTag"    : {
                "type": "string"
            },
            "timestamp": {
                "type": "string"
            },
            "newStatus": {
                "type": "boolean"
            },
            "oldStatus": {
                "type" "boolean"
            },
            "from"     : {
                "type": "string"
            },
            "to"       : {
                "type": "string"
            },
            "price"    : {
                "type": "number"
            },
            "invoice"  : {
                "type": "number"
            },
        },
        "required"  : ["vcTag", "timestamp"]
    },
    "log_index"   : "number",
    "tx_index"    : "number",
    "tx_hash"     : "string",
    "block_number": "number",
    "block_hash"  : "string",
    "address"     : "string",
    "created"     : "string"
}
