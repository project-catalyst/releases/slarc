from rest_framework import serializers

from api.models import SLA, SLO, Level, Event, SLOBillingHistory, Defaults, ClientToNotify


class EventSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Event
        fields = ('id', 'type', 'status', 'start_time', 'end_time', 'value', 'slo')


class LevelSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Level
        fields = ('id', 'status', 'min_value', 'max_value', 'price_drop_percentage', 'slo')


class SLOHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SLOBillingHistory
        fields = ('billing_time', 'start', 'end', 'value', 'status')


class SLOSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)
    events = EventSerializer(many=True, read_only=True)
    levels = LevelSerializer(many=True, read_only=True)
    history = SLOHistorySerializer(many=True, read_only=True)

    class Meta:
        model = SLO
        fields = (
        'id', 'objective', 'target_value', 'current_value', 'unit', 'status', 'sla', 'billing_cycle', 'last_billed',
        'next_bill', 'levels', 'events', 'history')


class SLASerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)
    slos = SLOSerializer(read_only=True, many=True)

    class Meta:
        model = SLA
        fields = ('id', 'entity_type', 'entity_id', 'valid_from', 'valid_until', 'slos')


class GeneralSerializer(serializers.Serializer):
    class Meta:
        model = None


class DefaultsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Defaults
        fields = ('slo_billing_cycle', 'slo_objective', 'slo_target_value', 'slo_unit', 'ctn_protocol', 'ctn_server',
                  'ctn_port', 'ctn_service_url', 'ctn_percentage', 'sla_duration')


class ClientToNotifySerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientToNotify
        fields = '__all__'