import logging.config

from dateutil.parser import parse
from dateutil.relativedelta import *
from django.conf import settings
from django.utils import timezone

from api.models import SLO, SLOBillingHistory, Defaults, SLA, Level, ClientToNotify

logging.config.dictConfig(settings.LOGGING)
log = logging.getLogger("api")


def reset_slo_value(billing_cycle):
    """
    Resets the SLO value (due to billing cycle)
    Args:
        billing_cycle: The billing cycle ("daily", "weekly", "monthly", "yearly")

    Returns:
        None
    """
    log.debug("Checking billing cycle reset for {} SLOs.".format(billing_cycle))
    slos = SLO.objects.filter(billing_cycle=billing_cycle).exclude(status='completed')
    for slo in slos:
        now = timezone.now()
        slo.calculate_value(reference_end=now, freeze_ongoing_events=True)
        history_item = SLOBillingHistory.objects.create(
            slo=slo,
            billing_time=now,
            value=slo.current_value,
            start=slo.next_bill,
            end=now,
            status=slo.status
        )
        history_item.save()
        slo.current_value = 0
        slo.status = 'active'
        slo.last_billed = now
        slo.next_bill = get_next_billing_time(billing_cycle, now)
        slo.save()
        slo.update_levels(recalculate=False)
        log.debug("Reset value for SLO {}.".format(slo.id))


def get_next_billing_time(billing_cycle, now):
    if "daily" == billing_cycle:
        return now + relativedelta(hours=+24)
    elif "weekly" == billing_cycle:
        return now + relativedelta(weeks=+1)
    elif "monthly" == billing_cycle:
        return now + relativedelta(months=+1)
    elif "yearly" == billing_cycle:
        return now + relativedelta(years=+1)
    else:
        raise ValueError("Billing cycle {} not recognized".format(billing_cycle))


def create_default_chain_for_entity(entity_type, entity_id, valid_from=None):
    """
    Creates a new, default SLA chain for an entity
    Args:
        entity_type: The entity type to create for
        entity_id: The entity id

    Returns:

    """
    defaults = Defaults.objects.all().last()

    # Set the SLA
    if valid_from:
        valid_from = parse(valid_from).replace(tzinfo=timezone.utc)
    else:
        valid_from = timezone.now() + relativedelta(minutes=-10)
    if defaults.sla_duration == "year":
        valid_until = valid_from + relativedelta(years=1)
    elif defaults.sla_duration == "month":
        valid_until = valid_from + relativedelta(months=1)
    else:
        valid_until = valid_from + relativedelta(weeks=1)

    sla = SLA.objects.create(
        entity_type=entity_type,
        entity_id=entity_id,
        valid_from=valid_from,
        valid_until=valid_until
    )
    sla.save()

    # Create the SLO
    billing_cycle = defaults.slo_billing_cycle
    if billing_cycle == "daily":
        next_bill = valid_from + relativedelta(days=+1)
    elif billing_cycle == "weekly":
        next_bill = valid_from + relativedelta(weeks=+1)
    elif billing_cycle == "monthly":
        next_bill = valid_from + relativedelta(months=+1)
    else:
        next_bill = valid_from + relativedelta(years=1)

    objective = defaults.slo_objective
    target_value = defaults.slo_target_value
    unit = defaults.slo_unit

    slo = SLO.objects.create(
        objective=objective,
        target_value=target_value,
        current_value=0,
        unit=unit,
        billing_cycle=billing_cycle,
        last_billed=valid_from,
        next_bill=next_bill,
        sla=sla
    )
    slo.save()

    # Create the levels
    # By default, we get two levels
    # Level 1: 0-Target, no price drop
    # Level 2: Target-00, 100% price drop
    l1 = Level.objects.create(
        min_value=0,
        max_value=target_value,
        price_drop_percentage=0,
        slo=slo,
        status='active'
    )
    l2 = Level.objects.create(
        min_value=target_value,
        max_value=0,
        price_drop_percentage=100,
        slo=slo
    )
    l1.save()
    l2.save()

    # Create default notification client
    ctn = ClientToNotify(
        sla=sla,
        responsible=defaults.ctn_server,
        protocol=defaults.ctn_protocol,
        server=defaults.ctn_server,
        port=defaults.ctn_port,
        service_url=defaults.ctn_service_url,
        percentage=defaults.ctn_percentage,
        last_notified=valid_from
    )
    ctn.save()
    log.info("Created new SLA stack ({}) for {} {}".format(sla.id, entity_type, entity_id))
    return True
