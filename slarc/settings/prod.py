from .base import *

DEBUG = False

# ==================================
#   DATABASE SETTINGS
# ==================================
POSTGRES_DATABASE = os.getenv('POSTGRES_DATABASE', default='slarc')
POSTGRES_USER = os.getenv('POSTGRES_USER', "slarc")
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', "slarc")
POSTGRES_HOST = os.getenv('POSTGRES_HOST', "localhost")
POSTGRES_PORT = os.getenv('POSTGRES_PORT', default=5432)

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.postgresql',
        'NAME'    : POSTGRES_DATABASE,
        'USER'    : POSTGRES_USER,
        'PASSWORD': POSTGRES_PASSWORD,
        'HOST'    : POSTGRES_HOST,
        'PORT'    : POSTGRES_PORT,
    }
}
