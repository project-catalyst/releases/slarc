from .base import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.postgresql',
        'NAME'    : 'slarc',
        'USER'    : 'slarc',
        'PASSWORD': 'slarc',
        'HOST'    : 'localhost',
        'PORT'    : 5432,
    }
}
