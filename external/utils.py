import logging

from dateutil.parser import parse
from django.utils import timezone

from external.models import VCGEvent

log = logging.getLogger('api')


def save_vcg_event(e):
    return VCGEvent.objects.create(
        event=None,
        name=e['name'],
        log_index=e['log_index'],
        tx_index=e['tx_index'],
        tx_hash =e['tx_hash'],
        block_number=e['block_number'],
        block_hash=e['block_hash'],
        address=e['address'],
        created=parse(e['created']).replace(tzinfo=timezone.utc),
        owner=e['args']['owner'] if 'owner' in e['args'] else None,
        vc_tag=e['args']['vcTag'] if 'vcTag' in e['args'] else None,
        timestamp=parse(e['args']['timestamp']) if 'timestamp' in e['args'] else timezone.now(),
        available=e['args']['newStatus'] if 'newStatus' in e['args'] else True,
        from_dc=e['args']['from'] if 'from' in e['args'] else None,
        to_dc=e['args']['to'] if 'to' in e['args'] else None
    )


def vcg_event_to_dict(e):
    return {
        "id": 0,
        "name": e.name,
        "args": {
            "owner": e.owner,
            "vcTag": e.vc_tag,
            "timestamp": e.timestamp.replace(tzinfo=timezone.utc).isoformat(),
            "newStatus": e.available,
            "oldStatus": not e.available,
            "from": e.from_dc,
            "to": e.to_dc
        },
        "log_index": e.log_index,
        "tx_index": e.tx_index,
        "tx_hash": e.tx_hash,
        "block_number": e.block_number,
        "block_hash": e.block_hash,
        "address": e.address,
        "created": e.created.replace(tzinfo=timezone.utc).isoformat()
    }