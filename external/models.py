from django.db import models

# Create your models here.
from django.utils import timezone

from api.models import Event


class VCGEvent(models.Model):
    event = models.ForeignKey(Event, null=True, blank=True, on_delete=models.CASCADE, default=None, related_name="vcg_events", help_text="The SLARC event corresponding to this one")
    name = models.CharField(max_length=40, null=False, blank=False, default="Unknown", help_text="The event type")
    log_index = models.IntegerField(default=0, null=False, blank=False, help_text="The log index in the Tx")
    tx_index = models.IntegerField(default=0, null=False, blank=False, help_text="The Tx index in the block")
    tx_hash = models.CharField(max_length=66, null=False, blank=False, default="0x0000000000000000000000000000000000000000000000000000000000000000", help_text="The Tx Hash")
    block_number = models.IntegerField(default=0, null=False, blank=False, help_text="The number of the block")
    block_hash = models.CharField(max_length=66, null=False, blank=False, default="0x0000000000000000000000000000000000000000000000000000000000000000", help_text="The block Hash")
    address = models.CharField(max_length=42, null=False, blank=False, default="0x0000000000000000000000000000000000000000", help_text="The address")
    created = models.DateTimeField(null=False, blank=False, default=timezone.now, help_text="When the block entered the chain")
    owner = models.CharField(max_length=42, null=True, blank=True, help_text="The owner of the tag")
    vc_tag = models.CharField(max_length=66, blank=False, null=False, default="0x0000000000000000000000000000000000000000000000000000000000000000", help_text="The VC tag")
    timestamp = models.DateTimeField(null=False, blank=False, default=timezone.now, help_text="The timestamp of the event")
    available = models.BooleanField(null=True, blank=True, default=True, help_text="Whether the VC is available or not")
    from_dc = models.CharField(max_length=42, null=True, blank=True, help_text="The source DC")
    to_dc = models.CharField(max_length=42, null=True, blank=True, help_text="The destination DC")

    def __str__(self):
        return f"VCG {self.name} event for {self.vc_tag}"