FROM python:3.7-alpine

ENV PIPENV_VENV_IN_PROJECT=1
ENV PROJECT_ROOT=/opt/slarc

WORKDIR ${PROJECT_ROOT}
RUN pip3 install pipenv

RUN set -ex \
    && apk add --no-cache --virtual .build-deps make gcc libc-dev linux-headers  pcre-dev python3-dev musl-dev postgresql-dev

# Copy Pipfile & lock
COPY Pipfile* $PROJECT_ROOT/

RUN LIBRARY_PATH=/lib:/usr/lib /bin/sh -c "pipenv install" \
    && runDeps="$( \
            scanelf --needed --nobanner --recursive .venv \
                    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                    | sort -u \
                    | xargs -r apk info --installed \
                    | sort -u \
    )" \
    && apk del .build-deps \
    && apk add --no-cache $runDeps

# Copy the application code
COPY . .
COPY ./docker/entrypoint.sh ${PROJECT_ROOT}/entrypoint.sh

# Collect static files with `base` settings to bypass database access
RUN pipenv run python3 manage.py collectstatic --settings=slarc.settings.base --no-input

# Delete all package cache
RUN rm -rf /root/.cache
# Delete unnecessary files
RUN rm -rf docker

# Gunicorn configuration
# Full settings can be found here: http://docs.gunicorn.org/en/stable/settings.html
ENV GUNICORN_CMD_ARGS="--bind 0.0.0.0:80 --workers 8 --worker-class gevent --user 1001 -- group 1001 --error-logfile - --access-logfile -"

ENTRYPOINT ["sh", "entrypoint.sh"]

EXPOSE 80
