#!/usr/bin/env sh

echo "Waiting for DB to spawn..."
while ! nc -z ${POSTGRES_HOST} 5432; do
  sleep 0.1
done
echo "PostgreSQL started"


echo "Waiting for RabbitMQ to spawn..."
while ! nc -z ${RABBITMQ_HOST} 5672; do
  sleep 0.1
done
echo "RabbitMQ started"

if [ "${IS_API}" == "true" ]; then
    pipenv run python manage.py migrate
    pipenv run python manage.py init
    pipenv run python manage.py init_rabbitmq
fi

exec "$@"